# Phoc

As of 2021-08-03 Upstream development moved to GNOME's gitlab. The new location for code
and issues is at https://gitlab.gnome.org/World/Phosh/phoc.

The packaging for the Librem 5 and PureOS
lives at https://source.puri.sm/Librem5/debs/pkg-phoc.
